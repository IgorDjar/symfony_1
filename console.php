#!/usr/bin/env php
<?php

spl_autoload_register(function ($className) {
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    include_once $_SERVER['DOCUMENT_ROOT'] . $className . '.php';
});

$sub = new Syberry\Academy\Controller\SubscriptionController();

$sub->cancelSubscriptionForUser(15);
