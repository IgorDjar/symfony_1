<?php
/**
 * Created by PhpStorm.
 * User: Djarik
 * Date: 26.03.2019
 * Time: 0:41
 */

namespace Syberry\Academy\Infrastructure\Http;

class Response
{
    /**
     * @var integer
     */
    private $code;

    private $data;

    /**
     * Response constructor.
     * @param int $code
     * @param $data
     */
    public function __construct($code, $data = [])
    {
        $this->code = $code;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
