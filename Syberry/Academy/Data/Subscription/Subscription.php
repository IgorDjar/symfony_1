<?php
/**
 * Created by PhpStorm.
 * User: Djarik
 * Date: 26.03.2019
 * Time: 0:40
 */

namespace Syberry\Academy\Data\Subscription;

class Subscription
{

    /**
     * @var SubscriptionPlan
     */
    private $plan;

    /**
     * @var integer
     */
    private $gatewayId;

    /**
     * Subscription constructor.
     * @param $planId int
     */
    public function __construct($id)
    {
        $this->plan = new SubscriptionPlan($id);
        $this->gatewayId = $id;
    }

    /**
     * @return SubscriptionPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }
}
