<?php
/**
 * Created by PhpStorm.
 * User: Djarik
 * Date: 26.03.2019
 * Time: 0:39
 */

namespace Syberry\Academy\Data;

class User
{
    private $id;

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}
