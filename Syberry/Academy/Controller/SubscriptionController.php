<?php
/**
 * Created by PhpStorm.
 * User: Djarik
 * Date: 26.03.2019
 * Time: 0:42
 */

namespace Syberry\Academy\Controller;

use Exception;
use Syberry\Academy\Data\User;
use Syberry\Academy\Infrastructure\Http\Response;
use Syberry\Academy\SubscriptionService;

class SubscriptionController
{
    private $subscriptionService;

    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->subscriptionService = new SubscriptionService();
    }

    public function cancelSubscriptionForUser($userId)
    {
        $user = new User($userId);
        try {
            $result = $this->subscriptionService->cancelSubscription($user);
        } catch (Exception $exception) {
            $exception->getMessage();
        }
        if (!$result) {
            return new Response(200);
        } else {
            http_response_code($result['code']);
            if ($result['key'] == SubscriptionService::FAILED_CANCEL_SUBSCRIPTION_KEY) {
                return new Response($result['code'], [
                    'message' => 'The subscription wasn\'t cancelled due to technical issue'
                ]);
            } else {
                if ($result['key'] == SubscriptionService::NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY) {
                    return new Response($result['code'], [
                        'message' => 'Subscription is not found for user'
                    ]);
                }
            }
            return null;
        }
    }
}
