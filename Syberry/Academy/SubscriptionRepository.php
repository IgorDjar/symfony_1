<?php
/**
 * Created by PhpStorm.
 * User: Djarik
 * Date: 26.03.2019
 * Time: 0:40
 */

namespace Syberry\Academy;

use Syberry\Academy\Data\Subscription\Subscription;
use Syberry\Academy\Data\User;

class SubscriptionRepository
{
    public function getActiveSubscription(User $user)
    {
        // emulate case
        if ($user->getId() % 10) {
            // Just a stub
            return new Subscription($user->getId());
        } else {
            return null;
        }
    }
}
