<?php
/**
 * Created by PhpStorm.
 * User: Djarik
 * Date: 26.03.2019
 * Time: 0:41
 */

namespace Syberry\Academy;

use Exception;
use Syberry\Academy\Infrastructure\Subscription\SubscriptionGatewayApi;

class SubscriptionService
{
    const FAILED_CANCEL_SUBSCRIPTION_KEY = 1;
    const NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY = 2;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionGatewayApi
     */
    private $subscriptionGatewayApi;

    /**
     * SubscriptionService constructor.
     */
    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->subscriptionGatewayApi = new SubscriptionGatewayApi();
    }

    public function cancelSubscription($user)
    {
        $activeSubscription = $this->subscriptionRepository->getActiveSubscription($user);
        if (!empty($activeSubscription)) {
            try {
                $this->subscriptionGatewayApi->cancel($activeSubscription->getGatewayId());
            } catch (Exception $exception) {
                $key = self::FAILED_CANCEL_SUBSCRIPTION_KEY;
                $code = 422;
                throw new Exception("Key: {$key}. Code: {$code}");
            }
        } else {
            $key = self::NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY;
            $code = 404;
            throw new Exception("Key: {$key}. Code: {$code}");
        }
    }
}
